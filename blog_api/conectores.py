from requests import Session
from requests.adapters import HTTPAdapter
from functools import wraps
from flask import jsonify
from werkzeug.exceptions import HTTPException, default_exceptions


class HTTPTimeoutAdapter(HTTPAdapter):
    def __init__(self, *args, **kw):
        self.timeout = kw.pop('timeout', 30.)
        super().__init__(*args, **kw)

    def send(self, request, **kw):
        timeout = kw.get('timeout')
        if timeout is None:
            kw['timeout'] = self.timeout
        return super().send(request, **kw)


def setup_connectors(app, name='default', **options):
    if not hasattr(app, 'extensions'):
        app.extensions = {}

    if 'connectors' not in app.extensions:
        app.extensions['connectors'] = {}
    session = Session()

    if 'auth' in options:
        session.auth = options['auth']

    headers = options.get('headers', {})
    if 'Content-Type' not in headers:
        headers['Content-Type'] = 'application/json'
    session.headers.update(headers)

    retries = options.get('retries', 3)
    timeout = options.get('timeout', 30)
    adapter = HTTPTimeoutAdapter(max_retries=retries, timeout=timeout)
    session.mount('http://', adapter)

    app.extensions['connectors'][name] = session
    return session


def get_connector(app, name='default'):
    return app.extensions['connectors'][name]


def return_json(f):
    @wraps(f)
    def wraper(*args, **kwargs):
        return jsonify(f(*args, **kwargs))
    return wraper

def JsonApp(app):
    def error_handling(error):
        if isinstance(error, HTTPException):
            result = {'code': error.code,
                      'description': error.description,
                      'message': str(error)}
        else:
            description = default_exceptions[500] = 'Error insterno de servidor'
            result = {'code': 500,
                      'description': description,
                      'message': str(error)}

        resp = jsonify(result)
        resp.status_code = result['code']
        return resp

    for code in default_exceptions.keys():
        app.register_error_handler(code, error_handling)
    return app
