from . import mongo
from datetime import datetime
import uuid


class User(mongo.Document):

    username = mongo.StringField(
        required=True,
        max_legth=40,
    )
    password = mongo.BinaryField(
    )
    email = mongo.StringField()
    roles = mongo.ListField()
    public_id = mongo.StringField(default=str(uuid.uuid5))
    pago = mongo.ListField(mongo.GenericReferenceField())
    gestiona = mongo.ListField(mongo.ObjectIdField())
    acceso = mongo.ListField(mongo.StringField())
    meta = {
        'collection': 'usuarios',
        'indexes': [
            'username',
            'email',
            'roles',
        ]
    }


class Comment(mongo.Document):
    user = mongo.ReferenceField(User, reverse_delete_rule=mongo.CASCADE)
    date = mongo.DateTimeField(default=datetime.now)
    comment = mongo.StringField()
    meta = {
        'ordering': ['-date'],
        'collection': 'comments',
        'indexes': [
            'user',
            'date',
        ]
    }


class Post(mongo.Document):
    title = mongo.StringField(required=True)
    content = mongo.StringField(required=True)
    user = mongo.ReferenceField(User)
    date = mongo.DateTimeField(default=datetime.now)
    comments = mongo.ListField(mongo.ReferenceField(Comment))
    tags = mongo.ListField(mongo.StringField())
    meta = {
        'ordering': ['-date'],
        'collection': 'blog',
        'indexes': [
            'title',
            'user',
            'date',
            'tags',
        ]
    }
