from flask import Flask
from flask_mongoengine import MongoEngine

mongo = MongoEngine()


def create_app(objecter):
    app = Flask(__name__)
    app.config.from_object(objecter)
    mongo.init_app(app)

    from .controllers import blog_api_blueprint
    app.register_blueprint(blog_api_blueprint)

    return app
