import datetime
from collections import Counter
from flask import Blueprint, request, current_app
from werkzeug.local import LocalProxy
from mongoengine.errors import DoesNotExist
from .models import Comment, Post, User
from .conectores import return_json

logger = LocalProxy(lambda: current_app.logger)

blog_api_blueprint = Blueprint(
    'blog_api',
    __name__)

def sidebar():
    posts = Post.objects.only('title').order_by('-date').limit(10)
    order_post = [{'id': str(x.id), 'title': x.title}
                  for x in posts
                  if posts
                  ]
    tags = Post.objects.only('tags')
    cont_tags = dict(Counter([y for x in tags for y in x.tags]))
    result = {'posts': order_post, 'tags': cont_tags}
    return result

@blog_api_blueprint.route('/api', methods=['GET'])
@return_json
def blogs_home():
    posts = Post.objects()
    bar = sidebar()
    if posts:
        try:
            lista = [{'id': str(x.id),
                      'title': x.title,
                      'content': x.content,
                      'date': x.date,
                      'tags': x.tags,
                      'user': x.user.username} for x in posts if x.user]
            print({'posts': lista, 'sidebar': bar})
            return {'posts': lista, 'sidebar': bar}
        except DoesNotExist as e:
            return {'mensaje': f'no existen los usuarios de esos docs {e.args}'}

    return {'mensaje': 'Aun no se publica ningun post'}


@blog_api_blueprint.route('/api', methods=['POST'])
@return_json
def create_post():
    data = request.get_json()
    if data:
        title = data['title']
        content = data['content']
        user = data['user']
        tags = data['tags']
        if tags:
            tags = tags.split(',')
            tags = [x.strip() for x in tags]
        user = User.objects(id=user).first()
        logger.info(user)
        if user:
            post = Post(title=title, content=content, user=user)
            for x in tags:
                post.tags.append(x.strip())
            post.save()
            logger.info(post.__dict__)
            return {'mensaje': 'procesado post',
                    'post_id': str(post.id)}
        return {'mensaje': 'usuario no existe'}
    return {'mensaje': 'Sin parametros en el header de la request'}


@blog_api_blueprint.route('/api/<post_id>', methods=['PUT'])
@return_json
def update_blog(post_id):
    data = request.get_json()
    if data:
        post = Post.objects(id=post_id).first()
        if data['user'] != str(post.user.id):
            return {'mensaje': 'solo los autores des sus post pueden editarlos'}
        if post:
            post.title = data['title']
            post.content = data['content']
            post.tags = []
            for x in data['tags'].split(','):
                post.tags.append(x.strip())
            post.save()

            return {'mensaje': 'Post Actualizado', 'post_id': post_id}
        return {'mensaje': 'Post inexistente'}
    return {'mensaje': 'Debes introducir los parametros a actualizar'}


@blog_api_blueprint.route('/api/<post_id>', methods=['GET'])
@return_json
def view_blog(post_id):
    try:
        post = Post.objects(id=post_id).first()
        if post:
            bar = sidebar()
            elapsed = (datetime.datetime.now() - post.date).days
            comm = post.comments

            data = {'post': post,
                    'username': post.user.username,
                    'elapsed': elapsed,
                    'sidebar': bar}
            if comm:

                comments = [{'id': str(x.id),
                             'username': x.user.username,
                             'userid': str(x.user.id),
                             'date': (datetime.datetime.now() - x.date).days,
                             'comment': x.comment} for x in comm]
                data['comments'] = comments
            print(data)
            return data
    except Exception:
        return {"mensaje": "Post no encontrado"}


@blog_api_blueprint.route('/api/comment/<post_id>', methods=['POST'])
@return_json
def comment_post(post_id):
    post = Post.objects(id=post_id).first()
    if post:
        try:
            data = request.get_json()
            comment = Comment()
            user = User.objects(id=data.get('user')).first()
            comment.user = user
            comment.date = datetime.datetime.now()
            comment.comment = data.get('comment')
            comment.save()
        except DoesNotExist as e:
            return {'mensaje': 'Usuario no existe'}
        Post.objects(id=post_id).update_one(add_to_set__comments=comment)
        return {'mensaje': 'Comentario insertado'}
    return {'mensaje': 'Post no encontrado'}


@blog_api_blueprint.route('/api/<post_id>', methods=['DELETE'])
@return_json
def borra_post(post_id):
    post = Post.objects(id=post_id).first()
    if post:
        post.delete()
        return {'mensaje': 'Post Eliminado'}
    return {'mensaje': 'Post no existe'}


@blog_api_blueprint.route('/api/tag/<tag_name>', methods=["GET"])
@return_json
def busca_tag(tag_name):
    posts = Post.objects(tags__contains=tag_name).order_by('-date')
    if posts:
        bar = sidebar()
        try:
            lista = [{'id': str(x.id),
                      'title': x.title,
                      'content': x.content,
                      'date': x.date,
                      'tags': x.tags,
                      'user': x.user.username} for x in posts]
            return {'posts': lista, 'sidebar': bar}
        except DoesNotExist as e:
            return {'mensaje': 'no existen los usuarios de esos docs'}

    return {'mensaje': 'Aun no se publica ningun post'}


@blog_api_blueprint.route('/api/user/<user_id>')
@return_json
def busca_post_usuario(user_id):
    user = User.objects(id=user_id).first()
    if user:
        bar = sidebar()
        posts = Post.objects(user=user).order_by('-date')
        if posts:
            try:
                lista = [{'id': str(x.id),
                          'title': x.title,
                          'content': x.content,
                          'date': x.date,
                          'tags': x.tags,
                          'user': x.user.username} for x in posts]
                return {'posts': lista, 'sidebar': bar}
            except DoesNotExist as e:
                return {'mensaje': 'no existen los usuarios de esos docs'}

    return {'mensaje': 'Aun no se publica ningun post'}
