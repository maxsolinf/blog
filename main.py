from blog_api import create_app
import os


if not os.environ.get('BLOG_API_CONFIG'):
    os.environ['BLOG_API_CONFIG'] = 'DevConfig'
direct = os.path.curdir
config_dir = f'config.{os.environ["BLOG_API_CONFIG"]}'

app = create_app(config_dir)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
