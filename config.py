import os

"""Buscamos las Variables De Entorno y Configuramos aplicacion """

if os.environ.get('SECRET_KEY'):
    secret = os.environ.get('SECRET_KEY')
else:
    secret = '\xce\x1cE@\x06\xd4Cr\x87g\xf6Y\xb3U\xdb\x7f\x9c\xcb\x92\xd0H\xf2\xd8\xfc\xf3\x07~A\xe7Ro\xcb\xb8`vz\x8d\x02m^\xd2\x97z\xef'

if os.environ.get('DEBUG'):
    debug = os.environ.get('DEBUG')
else:
    debug = True

mongo = {'db': 'prueba', 'host': 'localhost', 'port': 10001}

for param in mongo.keys():
    if param == 'port':
        mongo[param] = int(os.environ.get(param.upper(), 27017))
    elif os.environ.get(param.upper()):
        mongo[param] = os.environ.get(param.upper())
    else:
        continue


class Config(object):
    """ Recordar configurar la secret key mediante variable de entorno para
        mayor seguridad, las variables de entorno serian 
    Variables Entorno:
        SECRET_KEY:
        DB:     Base de datos a la Cual conectaremos los modelos.
        HOST:   Host Mongodb para coneccion DB.
        PORT:   Puerto Mongodb para coneccion DB.
        DEBUG:  (True or False) Modo Depuracion False para produccion.
    """
    SECRET_KEY = secret
    DEBUG = True


class DevConfig(Config):
    MONGODB_SETTINGS = mongo
    DEBUG = debug
